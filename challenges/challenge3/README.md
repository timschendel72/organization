# Challenge 3

Deadline: 2024-03-07 12:00 MEZ

## Part 1: Select databases for the banking architecture (3 pts)

After considering your inputs, the bank has settled for a rough high-level architecture for their new banking systems.
You can find their new architectural diagram in `challenges/challenge3/banking-architecture.png`.

In this next iteration of the architectural design, the bank asks you to decide what databases to use for the services.

Draw an architectural diagram that includes the databases used by the system.
Show what kind of database (relational database, Document DB, key-value stores, fileshares, etc.) which component(s) would use. 
You can use the existing diagram file in `challenges/challenge3/banking-architecture.excalidraw` (open it with https://excalidraw.com/).

Within your `banking-architecture` repository, [tag the currently latest commit](https://docs.gitlab.com/ee/user/project/repository/tags/#create-a-tag) from challenge 2.
Tags allow you to give a state of the repository a specific name so that it's easier to find it later.
The tag should be named `challenge2`.

> We do this so that you can find the state the `banking-architecture` repository was in after completing challenge 2.
This allows you to continue with challenge 3 but easily go back to the previous state if you need to.

After you created the tag, replace your architectural diagram from challenge 2 with the new architectural diagram including the databases.
You do not need to replace or update the documentation; you can remove it if you want to.

Prepapre a form-free 5 minute presentation of your database decisions for our next session and be ready to answer questions about it.

## Part 2: Implement database for Trading API (7 pts)

Your Trading API from challenge 2 needs a database to be able to store orders.
Additionally you want to test the ability to scale out, which means to run multiple stateless Trading API instances in parallel to be able to serve more concurrent requests.
Your setup will look like this:

![Trading API Architecture](challenges/challenge3/trading-api-architecture.png)

As you can see, two instances of your Trading API are supposed to run in parallel.
You can achieve this by simply starting the Trading API a second time in a different Terminal window.
Make sure to change the port the second instance listens to, because only one server can listen to one port at the same time.

Choose a database that in your opinion is suited to store the orders.
For example, you can use databases such as:
- [Redis](https://redis.io/docs/install/install-redis/install-redis-on-windows/) (blazing fast key-value store - requires WSL2 on Windows)
- [MongoDB](https://www.mongodb.com/docs/manual/installation/) (Document database)
- [Dragonfly](https://www.dragonflydb.io/docs/getting-started) (key-value store)
- [MySQL](https://dev.mysql.com/doc/refman/8.3/en/installing.html) (relational database)
- [PostgreSQL](https://www.postgresql.org/download/) (relational database)

Implement the connection to the database in your Trading API and store orders that get created by the clients.

The API specification stays the same as in challenge 2, but the behavior of your API changes:
- `POST /orders`:
    - Based on the request payload of the request, create a new order.
      The properties provided in the request (`symbol`, `assetType`, `amount`, `orderType`) must stay the same.
      The other properties can continue to be example values.
    - Assign the order a random `orderID`.
    - Store the order in your database.
    - Return the order in the HTTP response.
- `POST /orders/{orderId}/confirm`:
    - The client provides the order it wants to confirm in the request path.
    - If no order with that ID exists, the API should return status 404.
    - If an order with that ID exists, the API continues to return status 204.

We will update the [Trading API tests](https://gitlab.com/wwi22amb/trading-api-tests) within the next few days for you to test whether your API works as expected.

Before you commit your changes to your `trading-api` repository, make sure to tag the current state from challenge 2 as explained in Part 1 of this challenge.

In the README of your repository, additionally provide detailed step by step instructions on how to set up the database and
what else needs to be done to get both instances of your Trading API running and working.
