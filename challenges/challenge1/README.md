# Challenge 1

Deadline: 2024-02-22 12:00 MEZ

## Part 1: Set up your development environment (2 pts)

You will need a functional development environment for this and upcoming challenges. You have time until the next session to ensure your development environment suits your needs.
Please spend some time getting familar with how to use these tools. Don't be [Michael Scott](https://youtu.be/xTQ7vhtp23w?si=bqMbKhvMmz1ANAGQ&t=48).

- Enroll in our Slack. We sent an invitation link to your DHBW Mannheim mail address.
- Create a [Gitlab](https://gitlab.com) account and post your username in the corresponding Slack thread for us to give you access to your team's Gitlab group.
- Install [Git](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git). Running `git --version` in a terminal should then return the installed version of Git.
- Install [any code editor](https://github.com/zeelsheladiya/Awesome-IDEs) (IDE).
- Install [Node.js and npm](https://nodejs.org/en/download/current) (JavaScript runtime and package manager). You will need it for this and possibly upcoming challenges.

> Help, I'm using a company device and have no rights to install these tools.

Please try to get your hands on a private device where you have administrator privileges.
If that's not possiblem, some of the tools we use might be available through your company's software delivery center.

If that's also not the case, you could use some kind of virtual machine in the cloud.
Some cloud providers offer free development machines that come pre-installed with Git and IDEs which you can use in your browser, for example [Google Cloud Shell](https://cloud.google.com/shell/docs/launching-cloud-shell).

## Part 2: Get familiar with fork, clone, commit and push (4 pts)

We have created a demo HTTP server that will run on your local machine and responds to requests to localhost with "Hello, WWI22AMB!".
You can find the repository for that server [here](https://gitlab.com/wwi22amb/hello-http-server).

Create a [fork](https://www.w3schools.com/git/git_remote_fork.asp?remote=gitlab) of that repository so that you have your own copy in your team's Gitlab group,
for example `gitlab.com/wwi22amb/team1/hello-http-server`. Then [clone](https://github.com/git-guides/git-clone) the forked repository so that you have it on your local machine.

Modify the project so that when started, the served HTTP file shows "Hello, < your team >!" instead of "Hello, WWI22AMB!".
[Commit the changes](https://github.com/git-guides/git-commit) and [push](https://github.com/git-guides/git-push) them to the main branch.

## Part 3: Get familiar with merge requests (4 pts)

Now you want to work on a new feature for that server. It is supposed to serve an additional file called `song.html` that contains the lyrics of your favorite song.

For that you need to [create a new branch](https://git-scm.com/book/en/v2/Git-Branching-Basic-Branching-and-Merging) named `feat/song` where you make and commit your new changes.
Then push that new branch to the remote repository and create a merge request from the branch `feat/song` to the `main` branch. Other team members are now able to review these changes before they get merged.

> You could also directly commit and push your change in the main branch, but it is best practice to work with separate branches for separate features. We want to see you do that and will give you points for the existence of the merge request.
