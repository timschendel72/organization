# Code of Conduct

- Assume Best Intent
- Communicate synchronously & asynchronously
- Selbstständigkeit
- Pay attention because ignorance is expensive.
- KISS

## Assume best intent

Ihr kennt das: Jemand schreibt euch eine kurze und knappe Email oder spricht in einem ungewohnten Ton mit euch und diese Tatsache gibt euch zu denken. Möglicherweise mag der oder diejenige euch nicht mehr, es ist etwas vorgefallen oder ein Gerücht über euch scheint die Runde zu machen. Jeder kennt diese imaginären Dramen die sich in unseren Köpfen abspielen. Am Ende kommt dann raus, die Email war nur aufgrund von Zeitmangel oder Krankheit so knapp und kurz geschrieben, jemand hatte sich einfach nicht so gut gefühlt wie sonst und eure Sorgen waren absolut unberechtigt.

Was wir von euch möchten: 

- Geht *immer* von einer guten Absicht eures gegenüber aus.
- Lasst euch nicht von Kleinigkeiten ablenken.

Negative Gedanken oder irgendwelche imaginären Dramen in unseren Köpfen sind weder produktiv noch wirken sich diese positiv auf eure mentale Gesundheit aus. 

Mehr dazu: [https://mamagoesbeyond.com/assume-positive-intent/](https://mamagoesbeyond.com/assume-positive-intent/)

## Communicate synchronously & asynchronously

Gerade in aktuellen und turbulenten Zeiten wie dieser, inmitten einer globalen Pandemie und einer reils-remote Vorlesung, ist uns dieser Punkt besonders wichtig. Wir nutzen unterschiedlichste Tools (Slack, Gitlab, Meet, etc.) während dieser Vorlesung für die Kommunikation und das gemeinsame Arbeiten. Während Meet beispielsweise eher für eine synchrone Kommunikation ausgelegt ist, können Slack und Gitlab für asynchrone Kommunikation verwendet werden, **müssen** aber **nicht**. Wir haben oft die Erfahrung gemacht, dass insbesondere bei der Verwendung von Tools wie Slack schnell eine Erwartungshaltung entsteht dass unser Gegenüber doch zu antworten hat, wenn: "er denn schon online ist." 

Gerade im Software Engineering oder in "White-Collar" Jobs kann eine solche Erwartungshaltung relativ schnell zu einer Stresssituation führen. Dies gilt für beide Gesprächspartner. Der angeschriebene unterbricht mehrfach seine eigentliche Aufgabe um dieser Erwartungshaltung gerecht zu werden und der Versender wartet sehnsüchtig auf eine Antwort. Auch hier gilt "Assume best intent": Auch wenn Slack eine synchrone Kommunikation ermöglicht, die Erwartungshaltung bei einer Anfrage oder Nachricht via Slack sollte eine **asynchrone** Antwort sein. Benötigt Ihr von eurem Teammitglied eine sofortige Antwort oder habt mehrere aufeinanderfolgende Fragen, bieten sich hier Meetings in Meet an bei denen ein schneller Informationsaustausch gewährleistet ist. 

Mehr dazu: [https://about.gitlab.com/company/culture/all-remote/asynchronous/#when-to-use-asynchronous-and-synchronous-communication](https://about.gitlab.com/company/culture/all-remote/asynchronous/#when-to-use-asynchronous-and-synchronous-communication)

## Selbstständigkeit

> With great power comes great responsibility.

Wir sind der Meinung, dass Studenten im 6. Semester selbst in der Lage sind sich einen optimalen Arbeitsmodus auszusuchen und sich selbst zu organisieren.

Ihr seid für eure Arbeitsergebnisse als Team gemeinsam verantwortlich und könnt euch dementsprechend so organisieren.

## Pay attention because ignorance is expensive.

This one needs no explanation.

## KISS

> Keep it simple, stupid.

![general problem](https://imgs.xkcd.com/comics/the_general_problem.png)

Wir bewerten euch nicht nach eurer Komplexität, nach shiny-new technologies oder anderen banalen Dingen sondern nach euren Ergebnissen. Auch wenn euch der Weg manchmal "zu einfach" scheint, kann dieser oft doch der richtige sein.
